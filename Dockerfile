# © 2019 Paul John King (paul_john_king@web.de).  All rights reserved.
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License, version 3 as published by the
# Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

ARG PROJECT_NAME
ARG PROJECT_VERSION
ARG PROJECT_URL

ARG WORKBENCH_IMAGE="registry.gitlab.com/paul_john_king/docker.ubuntu_workbench:0.0.18_7efec42"

ARG LIBPCRE_DOWNLOADS="https://ftp.pcre.org/pub/pcre"
ARG LIBPCRE_VERSION="10.33"

ARG WORK_DIR="/work"
ARG DOWNLOAD_DIR="${WORK_DIR}/downloads"
ARG SOURCE_DIR="${WORK_DIR}/sources"
ARG TARGET_DIR="${WORK_DIR}/targets"

FROM "${WORKBENCH_IMAGE}" AS workbench

	ARG LIBPCRE_DOWNLOADS
	ARG LIBPCRE_VERSION

	ARG WORK_DIR
	ARG DOWNLOAD_DIR
	ARG SOURCE_DIR
	ARG TARGET_DIR

	ARG LIBPCRE_NAME="pcre2-${LIBPCRE_VERSION}"

	ARG LIBPCRE_ARCHIVE_SUFFIX="tar.gz"
	ARG LIBPCRE_ARCHIVE_NAME="${LIBPCRE_NAME}.${LIBPCRE_ARCHIVE_SUFFIX}"
	ARG LIBPCRE_ARCHIVE_URL="${LIBPCRE_DOWNLOADS}/${LIBPCRE_ARCHIVE_NAME}"
	ARG LIBPCRE_ARCHIVE_PATH="${DOWNLOAD_DIR}/${LIBPCRE_ARCHIVE_NAME}"

	RUN \
		set -e; \
		set -u; \
		mkdir -p \
			"${DOWNLOAD_DIR}" \
			"${SOURCE_DIR}" \
			"${TARGET_DIR}"; \
		curl --output "${LIBPCRE_ARCHIVE_PATH}" "${LIBPCRE_ARCHIVE_URL}"; \
		cd "${SOURCE_DIR}"; \
		tar \
			--extract \
			--file "${LIBPCRE_ARCHIVE_PATH}" \
			--strip-components "1"; \
		./configure \
			--prefix "${TARGET_DIR}"; \
		make install; \
		return;

FROM "scratch"

	ARG PROJECT_NAME
	ARG PROJECT_VERSION
	ARG PROJECT_URL

	ARG WORKBENCH_IMAGE

	ARG LIBPCRE_DOWNLOADS
	ARG LIBPCRE_VERSION

	ARG TARGET_DIR

	LABEL \
		project.name="${PROJECT_NAME}" \
		project.version="${PROJECT_VERSION}" \
		project.url="${PROJECT_URL}" \
		workbench.image="${WORKBENCH_IMAGE}" \
		libpcre.downloads="${LIBPCRE_DOWNLOADS}" \
		libpcre.version="${LIBPCRE_VERSION}"

	COPY --from="workbench" "${TARGET_DIR}" "/"
